if (process.env.NODE_ENV === "prod") {
  require("babel-polyfill");
}

// import the env
const env = require("./server/utils/envHandler").EnvHandler;
const dbHandler = require ("./server/utils/dbHandler");
const router = require ("./server/modules/scheduler/router");
const logger = require ("morgan");
const helmet = require("helmet");

// Common imports
const bodyParser = require("body-parser");
const express = require("express");
const app = express();

// when env is dev, log via morgan
if (process.env.NODE_ENV === "dev") {
  app.use(logger("dev"));
}

// Use helmet
app.use(helmet());

// parsing req/res body to json
app.use(bodyParser.json({ limit: "50mb" }));

// for parsing the url encoded data using qs library
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));

// CORS Support
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,POST");
  //recs.header('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization, auth-token, Content-Type, Content-Range, Content-Disposition, Content-Description');
  res.header(
    "Access-Control-Allow-Headers",
    "Origin,X-Requested-With,content-type,Authorization,auth-token,Accept,user_id, app_id"
  );
  next();
});

// Load the routes
app.use(router);

// adding err handling middleware, this is a post-call middleware
// errorHandler(app);

// open db connection before server starts
dbHandler.openConnection().then(
  db_details => {
    console.log(`Db is connected`);

    // start server on port
    app.listen(env().PORT, () => {
      console.log(`server listening on ${env().PORT} `);
    });
  },
  err => {
    console.log("error in opening the connection", err);
  }
);

// kill process when Ctrl+C is hit
process.on("SIGINT", () => {
  dbHandler.closeConnection() 
  console.log("bye bye !");
  process.exit();
});
