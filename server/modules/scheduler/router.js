var express = require('express');
var router = express.Router();
let scheduler = require('../scheduler/controller');

router.post('/create-scheduler',(req,res,next) => {
    scheduler.createSchedule(req,res);
})
    
router.post('/modify-scheduler',(req,res,next) => {
    scheduler.modifySchedule(req,res);
})

module.exports = router;

