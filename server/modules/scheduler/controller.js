let repository = require('../scheduler/repository')

/* author - Ashish Kumar Mishra
create date - 22-09-2019
version - 1.0
description - function to create a scheduler */

exports.createSchedule = (req, res) => {
  //Taking the body input
  let schedule = {
    serviceName: req.body.serviceName,
    requestType: req.body.requestType,
    requestURL: req.body.requestURL,
    contentType: req.body.contentType,
    requestBody: req.body.requestBody,
    recurrence: req.body.recurrence,
    hour: req.body.hour,
    minute: req.body.minute
  }
  if (!schedule.serviceName || !schedule.requestType || !schedule.requestURL || !schedule.contentType || !schedule.recurrence || !schedule.minute) {
    res.status(404).send({
      message: "One or more fields are empty!"
    })
  }
  else {
    const options = {
      method: schedule.requestType,
      body: schedule.requestBody,
      uri: schedule.requestURL,
      headers: {
        "Authorization": req.headers['authorization'],
        "Content-Type": schedule.contentType,
      },
      json: true
    }
    // Calls the
    repository.createScheduler(schedule, options)
      .then(result => {
        return res.status(result.statusCode).send({
          statusCode: result.statusCode,
          success: result.success,
          message: result.message,
          schedulerID: result.schedulerID
        })
      })
      .catch(err => {
        return res.status(400).send({
          err: err
        })
      })
  }
}

/* author - Ashish Kumar Mishra
create date - 22-09-2019
version - 1.0
description - function to modify a scheduler */

exports.modifySchedule = (req, res) => {
  //Taking the body input
  let schedule = {
    schedulerID: req.body.schedulerID,
    serviceName: req.body.serviceName,
    requestType: req.body.requestType,
    requestURL: req.body.requestURL,
    contentType: req.body.contentType,
    requestBody: req.body.requestBody,
    recurrence: req.body.recurrence,
    hour: req.body.hour,
    minute: req.body.minute
  }
  if (!schedule.serviceName || !schedule.requestType || !schedule.requestURL || !schedule.contentType || !schedule.recurrence || !schedule.minute) {
    res.status(404).send({
      message: "One or more fields are empty!"
    })
  }
  else {
    const options = {
      method: schedule.requestType,
      body: schedule.requestBody,
      uri: schedule.requestURL,
      headers: {
        "Authorization": req.headers['authorization'],
        "Content-Type": schedule.contentType,
      },
      json: true
    }
    // Calls the modifySchedular Repo function
    repository.modifyScheduler(schedule, options)
      .then(result => {
        return res.status(result.statusCode).send({
          statusCode: result.statusCode,
          success: result.success,
          message: result.message,
          schedulerID: result.schedulerID,
        })
      })
      .catch(error => {
        console.log(error)
        return res.status(400).send({
          statusCode: 400,
          success: false,
          message: "Bad request"
        })
      })
  }
}
