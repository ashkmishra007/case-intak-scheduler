//let dbConfig = require('../../../env/mongo-config');
let mongoose = require('mongoose')
var ObjectId = require('mongoose').Types.ObjectId;


//let db = dbConfig.mongoDbConn;

let schedularRequestAudit = new mongoose.Schema(
    {
        requestServiceName: {
            type: String,
            require: true
        },
        requestBody: {
            type: JSON,
            require: true
        },
        requestTime: {
            type: Date,
            require: true
        },
        status: {
            type: String,
        },
        remark: {
            type: String
        }
    },
    {
        versionKey: false // You should be aware of the outcome after set to false
    }
)

let schedular = new mongoose.Schema(
    {
        serviceName: {
            type: String,
            require: true
        },
        requestType: {
            type: String,
            require: true
        },
        requestURL: {
            type: String,
            require: true
        },
        contentType: {
            type: String,
            require: true
        },
        requestBody: {
            type: JSON,
            require: true
        },
        recurrence: {
            type: Boolean,
            require: true
        },
        status: {
            type: String,
            require: true
        },
        hour: {
            type: String,
            require: true
        },
        minute: {
            type: String,
            require: true
        },
        cron: {
            type: String,
            required: true
        }
    },
    {
        versionKey: false // You should be aware of the outcome after set to false
    }
);

let schedularProcessAudit = new mongoose.Schema(
    {
        requestID: {
            type: String,
            required: true
        },
        processTime: {
            type: Date,
            require: true
        },
        status: {
            type: String,
            require: true
        },
        remark: {
            type: String,
            require: true
        }
    },
    {
        versionKey: false // You should be aware of the outcome after set to false
    }
)

let schedularTransactionAudit = new mongoose.Schema(
    {
        processTime: {
            type: Date,
            require: true
        },
        status: {
            type: String,
            require: true
        },
        remark: {
            type: JSON,
            require: true
        }
    },
    {
        versionKey: false // You should be aware of the outcome after set to false
    }
)


const schedularModel = mongoose.model('schedular', schedular, 'schedular');
const schedularRequestAuditModel = mongoose.model('schedularRequestAudit', schedularRequestAudit, 'schedularRequestAudit');
const schedularProcessAuditModel = mongoose.model('schedularProcessAudit', schedularProcessAudit, 'schedularProcessAudit');
const schedularTransactionAuditModel = mongoose.model('schedularTransactionAudit', schedularTransactionAudit, 'schedularTransactionAudit');

// schedularRequestAudit stores the request audit details into the schedularRequestAudit collection
exports.saveSchedularRequestAudit = (schedule) => {
    let schedularRequestAudit = new schedularRequestAuditModel({
        requestServiceName: schedule.serviceName,
        requestBody: schedule.requestBody,
        requestTime: Date.now(),
        status: null,
        remark: null
    })
    return schedularRequestAudit.save()
        .then(result => {
            let schedularProcessAudit = {
                requestID: result._id,
                processTime: Date.now(),
                status: null,
                remark: null
            };
            return saveSchedulerProcessAudit(schedule, schedularProcessAudit)
        })
        .then(schedule => {
            return Promise.resolve(schedule);
        })
        .catch(err => {
            console.log(err)
            return Promise.reject({
                success: false
            });
        })
}

// // schedularProcessAudit stores the request audit details into the schedularProcessAudit collection
const saveSchedulerProcessAudit = (schedule, new_schedularProcessAudit) => {
    let schedularProcessAudit = new schedularProcessAuditModel({
        requestID: new_schedularProcessAudit.requestID,
        requestBody: new_schedularProcessAudit.requestBody,
        requestTime: Date.now(),
        status: 'Running',
        remark: null
    })
    return schedularProcessAudit.save()
        .then(result => {
            let schedularDetails = new schedularModel({
                serviceName: schedule.serviceName,
                requestType: schedule.requestType,
                requestURL: schedule.requestURL,
                contentType: schedule.contentType,
                requestBody: schedule.requestBody,
                recurrence: schedule.recurrence,
                status: schedule.status,
                hour: schedule.hour,
                minute: schedule.minute,
                cron: schedule.cron,
            })
            return schedularDetails.save();
        })
        .then(result => {
            return Promise.resolve({
                schedulerID: result._id
            })
        })
        .catch(err => {
            return Promise.reject();
        })
}

// saveSchedulerTransactionAudit stores the transaction audit details into the schedulerTransactionAudit collection
exports.saveSchedulerTransactionAudit = (transactionAudit) => {
    let schedularTransactionAudit = new schedularTransactionAuditModel({
        schedulerID: '1',
        processTime: Date.now(),
        status: transactionAudit.status,
        remark: transactionAudit.remark
    })
    schedularTransactionAudit.save()
        .then(result => {
            return true;
        })
        .catch(err => {
            console.log(err)
            return false;
        })
}

// getSchedular gets the schedular details from the collection
exports.getSchedularDetails = (schedular_id) => {
    return schedularModel.findById(schedular_id);
}


// updateSchedularDetails updates the schedular details in the collection
exports.updateSchedularDetails = (schedule) => {
    return schedularModel.updateOne({ _id: schedule.schedulerID }, schedule);
}