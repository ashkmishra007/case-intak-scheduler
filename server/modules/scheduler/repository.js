let model = require("../scheduler/model")
let schedular = require('node-schedule')
let cronLogic = require("../scheduler/cron")
let randomString = require('randomstring')

exports.createScheduler = (schedule, options) => {
  let unique_name = randomString.generate(7);
  schedule.cron = unique_name;
  return model.saveSchedularRequestAudit(schedule)
    .then(result => {
      schedule._id = result.schedulerID;
      const cron = cronLogic.scheduling(schedule, options)
      if (cron == false) {
        return Promise.reject({
          statusCode: 500,
          success: false,
          message: "Cron job failed",
        })
      }
      return Promise.resolve({
        statusCode: 200,
        success: true,
        message: "Task scheduled successfully",
        schedulerID: result.schedulerID
      })
    })
    .catch(error => {
      return Promise.reject({
        statusCode: 400,
        success: false,
        message: "Task scheduled UNsuccessfully",
        schedulerID: "Aaand"
      })
    })
}

exports.modifyScheduler = (schedule, options) => {
  return model.getSchedularDetails(schedule.schedulerID)
    .then(result => {
      cronJob = schedular.scheduledJobs[result.cron];
      schedule._id = result.schedulerID;
      if (cronJob === undefined) {
        schedule.cron = result.cron
        const cron = cronLogic.scheduling(schedule, options)
      if (cron == false) {
        return Promise.reject({
          statusCode: 500,
          success: false,
          message: "Cron job failed",
        })
      }      
    } else {
      const cronner = cronLogic.rescheduling(schedule, cronJob)
      if (cronner == false) {
        return Promise.reject({
          statusCode: 500,
          success: false,
          message: "Cron job failed",
        })
      }
    }
    return model.updateSchedularDetails(schedule)
      .then(result => {
        if(result) {
          return {
            statusCode: 200,
            success: true,
            message: "Task rescheduled successfully",
            schedulerID: schedule.schedulerID
          }
        }
      })
      .catch(error => {
        console.log(error)
        return {
          statusCode: 400,
          succe1ge: "Couldn't reschedule task"
        }
      })
    })
    .catch(error => {
      console.log(error)
      return {
        statusCode: 400,
        success: false,
        message: "Couldn't reschedule task",
        schedulerID: "Aaand"
      }
    })
}

