let schedular = require('node-schedule');
let request = require('request')
let model = require('./model')


let hour = 0;
let minute = 0;
let rule;

exports.scheduling = (schedule, options) => {
  // If scheduled to work in intervals
  if (schedule.recurrence == true) {
    rule = new schedular.RecurrenceRule();
    if (parseInt(schedule.minute) > 0) {
      rule.minute = new schedular.Range(0, 59, parseInt(schedule.minute));
    }
    else if (parseInt(schedule.minute) == 0) {
      rule.minute = null;
    }
    else {
      return {
        statusCode: 400,
        success: false,
        message: "Invalid minute time"
      }
    }
    if (parseInt(schedule.hour) > 0) {
      rule.hour = new schedular.Range(0, 11, parseInt(schedule.hour));
    }
    else if (parseInt(schedule.hour) == 0) {
      rule.hour = null;
    }
    else {
      return {
        statusCode: 400,
        success: false,
        message: "Invalid hour time"
      }
    }
  }
  // If scheduled to work on a particular time
  else {
    let when = new Date()
    rule = new Date(when.getFullYear, when.getMonth, when.getDate, hour, minute, 0);
  }
  let transactioAudit
  let scheduleQuery = schedular.scheduleJob(schedule.cron, rule, function () {
    request(options, (err, response, body) => {
      if (err) {

        transactioAudit = {
          schedularID: schedule._id,
          status: "Failed",
          remark: JSON.stringify(err)
        }
        model.saveSchedulerTransactionAudit(transactioAudit)
        return false;
      }
      else {
        transactioAudit = {
          schedularID: schedule._id,
          status: "Running",
          remark: JSON.stringify(scheduleQuery)
        }
        model.saveSchedulerTransactionAudit(transactioAudit)
        return true;
      }
    })
  })
}

exports.rescheduling = (schedule, cronJob) => {
  // If scheduled to work in intervals
  if (schedule.recurrence == true) {
    rule = new schedular.RecurrenceRule();
    if (parseInt(schedule.minute) > 0) {
      rule.minute = new schedular.Range(0, 59, parseInt(schedule.minute));
    }
    else if (parseInt(schedule.minute) == 0) {
      rule.minute = null;
    }
    else {
      return {
        statusCode: 400,
        success: false,
        message: "Invalid minute time"
      }
    }
    if (parseInt(schedule.hour) > 0) {
      rule.hour = new schedular.Range(0, 11, parseInt(schedule.hour));
    }
    else if (parseInt(schedule.hour) == 0) {
      rule.hour = null;
    }
    else {
      return {
        statusCode: 400,
        success: false,
        message: "Invalid hour time"
      }
    }
  }
  // If scheduled to work on a particular time
  else {
    let when = new Date()
    rule = new Date(when.getFullYear, when.getMonth, when.getDate, hour, minute, 0);
  }
  let scheduleQuery = cronJob.reschedule(rule);
  if (scheduleQuery === true) {
    var transactioAudit = {
      schedularID: schedule._id,
      status: "Running",
      remark: "Task is scheduled successfully"
    }
  }
  else if (scheduleQuery === false) {
    var transactioAudit = {
      schedularID: schedule._id,
      status: "Failed",
      remark: "Host unreachable"
    }
  }
  model.saveSchedulerTransactionAudit(transactioAudit)
}