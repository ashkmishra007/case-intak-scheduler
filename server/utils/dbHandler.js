const mongoose = require('mongoose');
const env = require('./envHandler');
const dburl = env.EnvHandler().dbUrl;
const db = mongoose.connection;
mongoose.Promise = global.Promise;

db.on('error', () => {
    console.log('error while communicating to database ');
});

exports.openConnection = () => {
    return mongoose.connect(dburl, { useNewUrlParser: true, useUnifiedTopology: true},);
}

exports.closeConnection = () => {
    db.close().then(() => {
        console.log('db disconnected');
    }).catch(() => {
        console.log('Could not close db connection');
    });
}

// exports.DBHandler = () => {
//     openConnection,
//     closeConnection
// };