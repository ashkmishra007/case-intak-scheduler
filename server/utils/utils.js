
function hasValue(obj, key) {
    return (obj.hasOwnProperty(key) && obj[key] !== 'undefined') ? true : false;
};

function checkParams(body, bodyArr) {
    for (let i = 0; i < bodyArr.length; i++) {
        if (!hasValue(body, bodyArr[i])) {
            return {
                success: false,
                value: bodyArr[i]
            };
        }
    }
    return {
        success: true
    };
}

const isRealString = (str) => typeof str === 'string' && str.trim().length > 0;

const removeLastComma = (str) => str.replace(/,\s*$/, "");

const checkUserIdAndAppId = (req) => req.headers.user_id && req.headers.app_id;

module.exports = {
    isRealString,
    checkParams,
    removeLastComma,
    checkUserIdAndAppId
};